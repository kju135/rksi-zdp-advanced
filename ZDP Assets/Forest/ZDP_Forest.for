A
800
FOREST

TEXTURE Trees.dds

LOD 10000
NO_SHADOW

SCALE_X 2048				
SCALE_Y 2048

SPACING 8 8
RANDOM 3 3

#	low-left	tex size	center	percent	--height--
# tree	s	t	w	y	offset	occur	min	max	quads	type	name
#------------------------------------------------------------------------------------------
TREE	0	0	1024   900	550	25	8	12	4	0	Tree1
TREE	1024	0	1024   950	500	25	8	12	4	0	Tree2
TREE	0	1024	1024   950	355	25	8	12	4	0	Tree3
TREE	1024	1024	1024   900	500	25	8	12	4	0	Tree4

SKIP_SURFACE water
