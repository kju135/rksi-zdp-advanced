# RKSI - Incheon International by Zero Dollar Payware

**Introduction**

This is an upgrade of ZDP's RKSI V1.0 Scenery by RoofSkY.

The repository is protected by the licenses in the ZDP RKSI.

For bug fixes or other inquiries, please contact the following discord id.

옥상하늘#9880

이 시너리는 옥상하늘이 ZDP의 RKSI V1.0 시너리를 업그레이드 한 시너리입니다.

이 레포지토리는 ZDP RKSI의 라이센스에 의해 보호됩니다.

버그 수정이나 다른 문의를 원할 경우 다음 디스코드로 연락해주세요.

옥상하늘#9880

## How to Install

### XP11

1. Download files from the release. 파일을 릴리즈에서 받아주세요.
2. Unzip the file you downloaded. 받은 Zip파일을 압축해제해주세요.
3. Rename the folder to "rksi-incheon". 압축해제한 폴더 이름을 "rksi-incheon" 으로 변경해주세요.
4. Go to your X-Plane Folder and Change File "\resources\default scenery\default apt dat\Earth nav data\apt.dat" to "apt.dat" files in this repository.

X-Plane 폴더로 가서 "\resources\default scenery\default apt dat\Earth nav data\apt.dat" 파일을 이 레포지토리에 있는 "apt.dat" 로 바꿔주세요.

5. Install SAM3 JetWays and MisterX Library. SAM3 JetWays와 MisterX 라이브러리를 설치해주세요
6. Align your Scenery and Have a Safe Flight! 시너리 정렬하면 끝!


### XP12

1. Download files from the release. 파일을 릴리즈에서 받아주세요.
2. Unzip the file you downloaded. 받은 Zip파일을 압축해제해주세요.
3. Rename the folder to "rksi-incheon". 압축해제한 폴더 이름을 "rksi-incheon" 으로 변경해주세요.
4. Go to your X-Plane Folder and Change File "\Global Scenery\Global Airports\Earth nav data\apt.dat" to "apt.dat" files in this repository.

X-Plane 폴더로 가서 "\Global Scenery\Global Airports\Earth nav data\apt.dat" 파일을 이 레포지토리에 있는 "apt.dat" 로 바꿔주세요.

5. Install SAM3 JetWays and MisterX Library. SAM3 JetWays와 MisterX 라이브러리를 설치해주세요
6. Align your Scenery and Have a Safe Flight! 시너리 정렬하면 끝!

**RoadMaps (Updated 2023.05.31)**

RWY 16R-34L : Texture Complete

Taxiway For 16R-34L : Work in Progress

Apron 2 : Work in Progress

Apron 4 : Work in Progress

Cargo Apron 2: Work in Progress

Cool Cargo Center : Work in Progress

**References**

South Korea AIP 2305

Google Maps

---

**[OFFICIAL FORUM THREAD](https://forums.x-plane.org/index.php?/forums/topic/231209-rksi-incheon-intl-by-zero-dollar-payware/&page=2&tab=comments#comment-2164747)**

**[Official ZDP Discord](https://discord.gg/aArWgU5)**

**Installation Instructions by ZDP**

Download and move the unzipped rksi-incheon folder into your custom scenery folder

압축 해제된 rksi-incheon 폴더를 다운로드하여 Custom Scenery Folder로 이동합니다.

Use xOrganizer, or delete your scenery_packs.ini to adjust your scenery load order if using our provided ortho.
If you have issues with the scenery loading, most likely your load order is wrong.

제공된 Ortho를 사용하는 경우 xOrganizer를 사용하거나 screen_pack.ini를 삭제하여 배경 로드 순서를 조정합니다.
시너리 로딩에 문제가 있는 경우 로드 순서가 잘못되었을 가능성이 높습니다.

**Required Libraries**

\*MisterX Library V2.0+

\*SAM Library V2+

This repository and it's contents are protected under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
Assets used from other developers was done so with their knowledge and approval.

이 리포지토리 및 리포지토리의 내용은 [CC BY-NC-ND 4.0]에 의해 보호됩니다.
다른 개발자들로부터 사용된 에셋은 그들의 승인을 얻어 수행되었습니다.

**Credits by ZDP**

ZDP Developers: StableSystem, Martini, Imran, awfsiu, TJ

Additional Considerations to the following people

YSLaurens#2763

瑜대객#0584

Tino

MisterX

Pyreegue

World Imagery (orthophotos)

Esri. "Imagery" [basemap]. Scale Not Given. "World Imagery". January 21, 2021. https://www.arcgis.com/home/item.html?id=10df2279f9684e4a9f6a7f08febac2a9. (February 18, 2021).

**Open Source Resources by ZDP**

This scenery is entirely open source and I encourage people to look through my source if you are curious how something is done or want to make modifications. If you modify something and think it should be implemented, please send me a message and I'd be happy to merge it into the official release.

이 시너리는 전적으로 오픈 소스이며 만약 어떻게 이루어지는지 궁금하거나 수정을 하고 싶다면 소스코드를 보는 것을 권장합니다. 만약 무언가를 수정해야 된다고 생각한다면, 메시지를 보내주시면 그것을 정식 릴리스에 병합하겠습니다.

RKSI full source - https://gitlab.com/StableSystem/rksi-incheon
